#include <stdbool.h>
#include <stm8s.h>
#include <stdio.h>
#include "main.h"
#include "milis.h"
#include "max7219.h"

#define INPUT_1 GPIOG, GPIO_PIN_2
#define INPUT_2 GPIOG, GPIO_PIN_3
#define INPUT_3 GPIOG, GPIO_PIN_1

#define OUTPUT_1 GPIOD, GPIO_PIN_6
#define OUTPUT_2 GPIOD, GPIO_PIN_5

#define DIN GPIOB, GPIO_PIN_4
#define CS GPIOB, GPIO_PIN_3
#define CLK GPIOB, GPIO_PIN_2

#define BTN BTN_PORT, BTN_PIN
#define BTN_1 GPIOD, GPIO_PIN_2
#define BTN_2 GPIOD, GPIO_PIN_0

#define OPENING true
#define CLOSING false

void display(uint8_t address, uint8_t data) {
    uint8_t mask;
    GPIO_WriteLow(CS); // BEGIN

    // ADRESS
    mask = 128;
    while (mask) {
        if (address & mask) {
            GPIO_WriteHigh(DIN);
        } else {
            GPIO_WriteLow(DIN);
        }
        GPIO_WriteHigh(CLK);
        mask = mask >> 1;
        GPIO_WriteLow(CLK);
    }
    // DATA
    mask = 128;
    while (mask) {
        if (data & mask) {
            GPIO_WriteHigh(DIN);
        } else {
            GPIO_WriteLow(DIN);
        }
        GPIO_WriteHigh(CLK);
        mask = mask >> 1;
        GPIO_WriteLow(CLK);
    }

    GPIO_WriteHigh(CS); // END
}

void display_Init(void)
{
    display(DECODE_MODE, 0b00000000);
    display(SCAN_LIMIT, 7);
    display(INTENSITY, 1);
    display(DISPLAY_TEST, DISPLAY_TEST_OFF);
    display(SHUTDOWN, SHUTDOWN_ON);
    for(uint8_t digit = 0; digit < 8; digit++)
        display(digit+1, 0xF);
}

void init(void)
{
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);

    GPIO_Init(INPUT_1, GPIO_MODE_IN_PU_NO_IT);
    GPIO_Init(INPUT_2, GPIO_MODE_IN_PU_NO_IT);
    GPIO_Init(INPUT_3, GPIO_MODE_IN_PU_NO_IT);

    GPIO_Init(OUTPUT_1, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(OUTPUT_2, GPIO_MODE_OUT_PP_HIGH_SLOW);

    GPIO_Init(DIN, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(CS, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(CLK, GPIO_MODE_OUT_PP_LOW_SLOW);

    GPIO_Init(BTN, GPIO_MODE_IN_FL_NO_IT);
    GPIO_Init(BTN_1, GPIO_MODE_IN_PU_NO_IT);
    GPIO_Init(BTN_2, GPIO_MODE_IN_PU_NO_IT);

    init_milis();
    display_Init();
}

void display_string(char string[])
{
    uint8_t characters[] = {
        0b01111110,  // 0
        0b00110000,  // 1
        0b01101101,  // 2
        0b01111001,  // 3
        0b00110011,  // 4
        0b01011011,  // 5
        0b01011111,  // 6
        0b01110010,  // 7
        0b01111111,  // 8
        0b01111011,  // 9
        0b00000000,  // ' '
        0b01110111,  // A
        0b00011111,  // B
        0b01001110,  // C
        0b00111101,  // D
        0b01001111,  // E
        0b01000111,  // F
        0b01011111,  // G
        0b00110111,  // H
        0b00000110,  // I
        0b00111100,  // J
        0b00000111,  // K
        0b00001110,  // L
        0b01010100,  // M
        0b01110110,  // N
        0b01111110,  // O
        0b01100111,  // P
        0b01110011,  // Q
        0b01000110,  // R
        0b01011011,  // S
        0b00001111,  // T
        0b00111110,  // U
        0b00111010,  // V
        0b00101010,  // W
        0b01001001,  // X
        0b00111011,  // Y
        0b01101001   // Z
    };

    uint8_t string_len = 0;
    uint8_t pos = 0;
    while(string[pos] != '\0')
    {
        string_len++;
        pos++;
    }

    uint8_t dots = 0;
    for(uint8_t digit = 0; digit<8+dots; digit++)
    {
        if(digit < string_len)
        {
            while(string[digit] == '.')
            {
                digit++;
                dots++;
            }

            uint8_t char_num = (char)string[digit];
            uint8_t offset = 48;
            if(char_num == 32)
                offset = 32-10;
            else if(char_num > 64)
                offset = 65-11;

            uint8_t dot = 0b00000000;
            if(string[digit+1] == '.')
            {
                dot = 0b10000000;
            }

            display(((7-digit)+1+dots), characters[char_num-offset] + dot);
        }
        else
        {
            display(((7-digit)+1+dots), 0);
        }
    }
}

int main(void)
{

    uint32_t time = 0, number = 0, timer_1 = 0;
    bool direction = CLOSING, pushed = false, running = false, timer_1_ON = false;

    // DISPLAY TEXT LIBRARY
    char strs [][] = {
        "CLOSED   ",
        "OPEN  .30",
        "OPEN 1.00",

        "OPENING  ",
        "CLOSING  "
    };

    init();

    // GET STARTING POSITION
    uint8_t state = 1;
    if(!GPIO_ReadInputPin(INPUT_1))
    {
        state = 2;
    }
    else if(!GPIO_ReadInputPin(INPUT_2))
    {
        state = 1;
    }
    else if(!GPIO_ReadInputPin(INPUT_3))
    {
        state = 0;
    }
    uint8_t to_state = state, str_num = state;

    while(1)
    {
        // RUN MOTOR
        if((milis() - timer_1 > 3000) && (timer_1_ON))
        {
            timer_1_ON = false;
            
            if (state == to_state)
            {
                str_num = state;
            }
            else
            {  
                if(state > to_state)
                    direction = CLOSING;
                else
                    direction = OPENING;

                if(direction == OPENING)
                    str_num = 3;
                else
                    str_num = 4;

                running = true;
            }
        }

        // EVERYTHING ELSE :)
        if (milis() - time > 50)
        {
            time = milis();

            display_string(strs[str_num]); // SET DISPLAY

            // GET STATE
            if(!GPIO_ReadInputPin(INPUT_1))
            {
                state = 2;
            }
            else if(!GPIO_ReadInputPin(INPUT_2))
            {
                state = 1;
            }
            else if(!GPIO_ReadInputPin(INPUT_3))
            {
                state = 0;
            }

            // BUTTONS
            if(((GPIO_ReadInputPin(BTN)) && (GPIO_ReadInputPin(BTN_1))) && (GPIO_ReadInputPin(BTN_2)))
            {
                pushed = false;
            }
            if(!pushed)
            {
                if (!running)
                {
                    if (!GPIO_ReadInputPin(BTN_1))
                    {
                        pushed = true;
                        if(to_state < 2)
                            to_state++;
                        str_num = to_state;
                        timer_1 = milis();
                        timer_1_ON = true;
                    }
                    
                    if (!GPIO_ReadInputPin(BTN_2))
                    {
                        pushed = true;
                        if(to_state > 0)
                            to_state--;
                        str_num = to_state;
                        timer_1 = milis();
                        timer_1_ON = true;
                    }
                }
                // STOP BUTTON
                if (!GPIO_ReadInputPin(BTN))
                {
                    pushed = true;

                    if (!running)
                    {
                        running = true;
                        direction = !direction;
                        if (direction == OPENING)
                            str_num = 4;
                        else
                            str_num = 3;
                    }
                    else
                    {
                        running = false;
                        str_num = state;
                    }
                }
            }

            // IS DONE?
            if(state == to_state)
            {
                running = false;
                str_num = state;
            }

            // SET RELAY
            if(running)
            {
                if(direction == OPENING)
                {
                    GPIO_WriteHigh(OUTPUT_1);
                    GPIO_WriteLow(OUTPUT_2);
                }
                else
                {
                    GPIO_WriteLow(OUTPUT_1);
                    GPIO_WriteHigh(OUTPUT_2);
                }
            }
            else
            {
                GPIO_WriteHigh(OUTPUT_2);
                GPIO_WriteHigh(OUTPUT_1);

                if (!timer_1_ON)
                    str_num = state;
            }
        }
    }
}

/*-------------------------------  Assert -----------------------------------*/
#include "__assert__.h"